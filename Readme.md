# Readme

To see output from modules:
cargo run -- --show-output

Need set linux max spawned processes to be more than default:
```sudo nano /etc/sysctl.conf```
Add this line to end:
```vm.max_map_count = 16777216```
