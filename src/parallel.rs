use rayon::prelude::*;
use std::collections::{HashMap, HashSet};
use std::sync::{Arc, Mutex};

use crate::config::{INTERVAL, NORMAL_MODE, NUMBER_MAX, SPECIAL_CASE};
use crate::utils::{get_all_possible_par, print_overlap_numb, save_csv, save_csv_2, sorted_by_key};

/// Create tuple for single combination, so it can be sorted
/// * `combination` - Combination used for evaluation
/// * `numbers` - occurrences value for each number
/// * `pairs` - occurrences value for each pair
/// * `triplets` - occurrences value for each triplet
/// Returns evaluated data tuple
fn evaluate_combination(
    combination: &(HashSet<u8>, Vec<String>, Vec<String>),
    numbers: &[u16; NUMBER_MAX],
    pairs: &Arc<HashMap<String, u16>>,
    triplets: &Arc<HashMap<String, u16>>,
) -> (HashSet<u8>, u16, u16, u16) {
    let sum_numb: u16 = combination
        .0
        .iter()
        .map(|&number| numbers[(number - 1) as usize])
        .sum();

    let mut sum_pair: u16 = 0;
    for comb_pair_set in combination.1.iter() {
        if pairs.contains_key(comb_pair_set) {
            sum_pair += pairs.get(comb_pair_set).unwrap()
        }
    }

    let mut sum_triple = 0;
    for comb_trip_set in combination.2.iter() {
        if triplets.contains_key(comb_trip_set) {
            sum_triple += triplets.get(comb_trip_set).unwrap()
        }
    }

    (combination.0.clone(), sum_numb, sum_pair, sum_triple)
}
/// Parallel calculate all possible combinations, and store result to file.
/// It also print valued position of one combination.
/// * `future_win` - Combination to to show its evaluated position and data
/// * `numbers` - Array in size of all possible numbers with occurrences
/// * `pairs` - Map of all possible pairs with their occurrences
/// * `triplets` - Map of all possible triplets with their occurrences
pub fn parallel_process(
    future_win: HashSet<u8>,
    numbers: [u16; NUMBER_MAX],
    pairs: HashMap<String, u16>,
    triplets: HashMap<String, u16>,
) {
    // Create and configure the process pool
    let all_possible: Vec<(HashSet<u8>, Vec<String>, Vec<String>)> = get_all_possible_par();

    let pairs = Arc::new(pairs);
    let triplets = Arc::new(triplets);
    let result_data = Arc::new(Mutex::new(Vec::new()));
    // Issue tasks to the process pool using Rayon's parallel iterator
    all_possible.par_iter().for_each(|combination| {
        let result: (HashSet<u8>, u16, u16, u16) =
            evaluate_combination(combination, &numbers, &pairs, &triplets);

        // Collect result
        let mut result_data = result_data.lock().unwrap();
        result_data.push(result);
    });

    //let sorted_by_numb = sorted_by_key(&result_data.lock().unwrap(), |x| x.1);
    //save_csv(sorted_by_numb, "result_numb.csv").unwrap();

    // Join parallel evaluation and sort by all columns
    // x.1 - triples occurrences
    // x.2 - pairs occurrences
    // x.3 - number occurrences
    let mut guard = result_data.lock().unwrap();
    let final_sorted = sorted_by_key(&guard, |x: &(HashSet<u8>, u16, u16, u16)| (x.3, x.2, x.1));

    if NORMAL_MODE {
        let len = final_sorted.len();
        for i in 0..=9 {
            println!(
                "Least occurring combination {}./{}: {:?}",
                i + 1,
                len,
                final_sorted[i]
            );
        }

        save_csv(final_sorted, "result_triple.csv").unwrap();
    } else {
        let mut winners = Vec::new();
        let mut values = Vec::new();

        match SPECIAL_CASE {
            0 => {
                // Go thought all combinations
                // combination 0 - set of numbers representing combination
                // combination 1 - numbers valuation
                // combination 2 - pairs valuation
                // combination 3 - triplet valuation
                for (i, combination) in final_sorted.iter().enumerate() {
                    if future_win.is_superset(&combination.0) {
                        println!(
                            "{:?},{:?},{:?},{:?},",
                            i, combination.1, combination.2, combination.3
                        );
                        winners.push(i);
                        values.push(combination.1);
                        values.push(combination.2);
                        values.push(combination.3);
                    }
                }

                println!("{:?}", future_win);
                save_csv_2(future_win, winners, values, "test.csv").unwrap();
            }
            1 => {
                let len = final_sorted.len();
                let len_i = len - 1;

                for i in 0..=INTERVAL {
                    print_overlap_numb(&future_win, &final_sorted[(i * len_i) / INTERVAL]);
                    print!(";");
                }
                println!();
            }
            _ => println!("Unexpected!"),
        }
    }

    guard.clear();
}
