use chrono::NaiveDate;
use chrono::Utc;
use csv::ReaderBuilder;
use csv::StringRecord;
use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::isize;

use crate::config::{NUMBERS_PULLED, BONUS_INDEX, NUMBER_MAX, SKIP_DATA_COLUMNS};
use crate::utils::arrays_equal;
use crate::utils::print_colored;
use crate::utils::Color;


fn is_any_bonus() -> bool {
    BONUS_INDEX > 0
}

/// Add/assign all values contained by single combination.
/// * `pairs` - `Updated` - assign value for each pair (dimension index == number in pair)
/// * `triplets` - `Updated` - assign value for each triplet (dimension index == number in triplet)
/// * pulled - How many numbers are pulled (win combination can be smaller than actual pull)
/// * offset - Data offset (TODO - deprecate)
fn add_groups(
    pairs: &mut [[u16; NUMBER_MAX]; NUMBER_MAX+1],
    triplets: &mut [[[u16; NUMBER_MAX]; NUMBER_MAX]; NUMBER_MAX],
    pulled: [u8; NUMBERS_PULLED],
    offset: u8,
) {
    let mut copy = pulled.to_vec();

    let mut bonus = is_any_bonus();
    
    for _ in 0..NUMBERS_PULLED {
        let x = copy.pop().unwrap();
        let xi = x - offset;
        let mut second_cp = copy.clone();

        for y in copy.iter() {
            let yi = y - offset;

            let mut keys_pair = vec![xi, yi];
            keys_pair.sort();

            pairs[keys_pair[0] as usize][keys_pair[1] as usize] += 1;

            //TODO more generic, bonus number has less value so bigger number
            if bonus {
                pairs[keys_pair[0] as usize][keys_pair[1] as usize] += 1;
            }

            second_cp.pop();

            for z in second_cp.iter() {
                let zi = z - offset;

                let mut keys_triplet = vec![xi, yi, zi];
                keys_triplet.sort();

                triplets[keys_triplet[0] as usize][keys_triplet[1] as usize]
                    [keys_triplet[2] as usize] += 1;

                //TODO more generic, bonus number has less value so bigger number
                if bonus {
                    triplets[keys_triplet[0] as usize][keys_triplet[1] as usize]
                        [keys_triplet[2] as usize] += 1
                }
            }
        }
        bonus = false;
    }
}

/// This method is used to verify if some data corruption occurred
/// * `row` - Single row of data extracted from CSV
/// * `offset` - offset used ( TODO - deprecate )
fn sanity_check(row: [u8; NUMBERS_PULLED], offset: u8) {
    let mut possible = [0; NUMBER_MAX];

    for number in row {
        let i = number - offset;
        if i >= NUMBER_MAX as u8 {
            panic!("Out of range");
        }

        if possible[i as usize] > 0 {
            panic!("Same Number");
        }

        possible[i as usize] += 1;
    }
}

/// Set each array numbers, pairs, triplets.
/// * `skip_last_x` - The number of rows to skip from the end of the CSV
/// * `numbers` - `Updated` - Each possible number will be set with its occurrence
/// * `pairs` - `Updated` - Each possible pair will be set with its occurrence
/// * `triplets` - `Updated` - Each possible triplet is set with its occurrence
/// * `file_path` - Filepath to CSV from which data will be extracted
/// Return lastSkipped combination (or empty array)
pub fn set_numbers(
    skip_last_x: isize,
    numbers: &mut [u16; NUMBER_MAX],
    pairs: &mut [[u16; NUMBER_MAX]; NUMBER_MAX+1],
    triplets: &mut [[[u16; NUMBER_MAX]; NUMBER_MAX]; NUMBER_MAX],
    file_path: &str,
) -> Result<[u8; NUMBERS_PULLED], Box<dyn Error>> {
    // Create a CSV reader
    let file = File::open(file_path)?;
    let mut reader = ReaderBuilder::new().from_reader(file);
    let mut row_count: isize = skip_last_x;
    for _ in reader.records() {
        row_count += 1;
    }

    // Create a CSV reader
    let file2 = File::open(file_path)?;
    let mut reader: csv::Reader<File> = ReaderBuilder::new().delimiter(b';').flexible(true).from_reader(file2);

    let mut skipped = false;
    let mut next_skipped: [u8; NUMBERS_PULLED] = [0; NUMBERS_PULLED];

    let mut prev_date: String = String::from("");
    let mut prev_extracted_data: [u8; NUMBERS_PULLED] = [0; NUMBERS_PULLED];

    let mut row: StringRecord;
    let mut current_date;

    // Iterate over each row in the CSV file
    for (csv_row_index, result) in reader.records().enumerate() {
        row = result?;
        current_date = row.get(1).unwrap();

        let mut extracted_data: [u8; NUMBERS_PULLED] = [0; NUMBERS_PULLED]; //TODO check if bigger than U8
        for (csv_row_index, numb) in row.iter().skip(SKIP_DATA_COLUMNS).take(NUMBERS_PULLED).enumerate() {
            extracted_data[csv_row_index] = numb.parse::<u8>()?;
        }

        // Check if it's the last row
        if row_count <= csv_row_index as isize {
            if !skipped {
                next_skipped = extracted_data;
                skipped = true
            }

            continue;
        }

        // Check for duplicate data in csv
        if prev_date == current_date && arrays_equal(&prev_extracted_data, &extracted_data) {
            print_colored(&format!("Skipped duplicate: {}!", prev_date), Color::Yellow);
            continue;
        }
        prev_date = current_date.to_string();
        prev_extracted_data = extracted_data;

        add_groups(pairs, triplets, extracted_data, 1);
        sanity_check(extracted_data, 1);

        for (csv_row_index, numb) in extracted_data.iter().enumerate() {
            numbers[(numb - 1) as usize] += 1;
            if csv_row_index == BONUS_INDEX {
                numbers[(numb - 1) as usize] += 1;
            }
        }
    }

    if !prev_date.is_empty() {
        let naive_current_date = Utc::now().naive_local().date();
        let naive_prev_date = NaiveDate::parse_from_str(&prev_date, "%d.%m.%Y").unwrap();
        let delta = naive_current_date.signed_duration_since(naive_prev_date);
        let max: i8 = 2;
        let days_delta = delta.num_days();
        if days_delta > max as i64 {
            print_colored(
                &format!("Time difference between last ({}) and current date ({}) is higher than expected {} days!", 
                    prev_date, 
                    naive_current_date.format("%d.%m.%Y"),
                    max),
                Color::Yellow,
            );
        } else {
            println!(
                "Time difference between last ({}) and current date ({}) is {} days",
                prev_date,
                naive_current_date.format("%d.%m.%Y"),
                days_delta
            );
        }
    }

    Ok(next_skipped)
}

/// Skips unnecessary data overhead and change data format to tuples
/// * `pairs` - Pairs data to transform
/// * `triplets` - Triplets data to transform
/// * `pair_tuples` - `Updated` - New pair data will be stored to this
/// * `triplet_tuples` - `Updated` - New triplet data will be stored to this
pub fn set_tuples(
    pairs: &[[u16; NUMBER_MAX]; NUMBER_MAX+1],
    triplets: &[[[u16; NUMBER_MAX]; NUMBER_MAX]; NUMBER_MAX],
    pair_tuples: &mut HashMap<String, u16>,
    triplets_tuples: &mut HashMap<String, u16>,
) {
    // Pairs transformation loop
    for (i, row) in pairs.iter().enumerate() {
        for (j, number) in row.iter().enumerate() {
            if i < j {
                let mut sorted = [(i + 1) as u8, (j + 1) as u8];
                sorted.sort();
                pair_tuples.insert(sorted.iter().map(|&num| num.to_string()).collect(), *number);
            }
        }
    }

    // Triplets transformation loop
    for (i, area) in triplets.iter().enumerate() {
        for (j, row) in area.iter().enumerate() {
            for (k, number) in row.iter().enumerate() {
                if i >= j || j >= k || i >= k {
                    continue;
                }
                let mut sorted = [(i + 1) as u8, (j + 1) as u8, (k + 1) as u8];
                sorted.sort();
                triplets_tuples
                    .insert(sorted.iter().map(|&num| num.to_string()).collect(), *number);
            }
        }
    }
}
