use chrono::Utc;
use csv::Writer;
use itertools::Itertools;
use rayon::iter::{ParallelBridge, ParallelIterator};
use std::{collections::HashSet, error::Error, fs::OpenOptions};

use crate::config::{NUMBERS_GUESSED, NUMBER_MAX};

pub enum Color {
    Red,
    Green,
    Yellow,
    Blue,
    // Add more colors as needed
}

/// Check if two arrays are equal
/// * `arr1` - 1. array
/// * `arr2` - 2. array
pub fn arrays_equal<T>(arr1: &[T], arr2: &[T]) -> bool
where
    T: PartialEq,
{
    arr1.len() == arr2.len() && arr1.iter().zip(arr2.iter()).all(|(a, b)| a == b)
}

/// Sorts vector
/// * `vector` - Vector to sort
fn sort_map(vector: Vec<u8>) -> String {
    let mut clone = vector.clone();
    clone.sort();
    clone.into_iter().map(|numb| numb.to_string()).collect()
}

/// Generate all possible combinations
pub fn get_all_possible_par() -> Vec<(HashSet<u8>, Vec<String>, Vec<String>)> {
    (1u8..(NUMBER_MAX + 1) as u8)
        .combinations(NUMBERS_GUESSED)
        .par_bridge()
        .map(|combination| {
            {
                (
                    combination.clone().into_iter().collect::<HashSet<u8>>(),
                    combination
                        .clone()
                        .into_iter()
                        .combinations(2)
                        .map(|double_combination| sort_map(double_combination))
                        .collect(),
                    combination
                        .into_iter()
                        .combinations(3)
                        .map(|triple_combination| sort_map(triple_combination))
                        .collect(),
                )
            }
        })
        .collect()
}

/// Sort data by callback with 3 params
/// * `data` - Data to be sorted
/// * `key_fn` - Sort callback, that refers to keys to be used for sorting
pub fn sorted_by_key<F, T>(
    data: &[(HashSet<u8>, u16, u16, u16)],
    key_fn: F,
) -> Vec<(HashSet<u8>, u16, u16, u16)>
where
    F: Fn(&(HashSet<u8>, u16, u16, u16)) -> T,
    T: Ord,
{
    let mut sorted_data: Vec<_> = data.to_vec();
    sorted_data.sort_by_key(key_fn);
    sorted_data
}

/// Saves NORMAL_MODE data to CSV, creates header row too
/// * `data` - Data vector to be saved to CSV
/// * `filename` - Name of csv file to save data to
pub fn save_csv(
    data: Vec<(HashSet<u8>, u16, u16, u16)>,
    filename: &str,
) -> Result<(), Box<dyn Error>> {
    let mut writer = Writer::from_path(filename)?;

    // Write header
    writer.write_record(&["Combination", "SumNumb", "SumPair", "SumTriple"])?;

    // Write data
    for (combination, sum_numb, sum_pair, sum_triple) in data {
        writer.write_record(&[
            combination
                .iter()
                .map(|&x| x.to_string())
                .collect::<Vec<String>>()
                .join(", "),
            sum_numb.to_string(),
            sum_pair.to_string(),
            sum_triple.to_string(),
        ])?;
    }

    writer.flush()?;
    Ok(())
}

/// Saves Not NORMAL_MODE data to CSV file
/// * `combination` - win combination
/// * `winners_no` - vector of winner positions
/// * `values` - winners values of occurrences
/// * `filename` - Name of csv file to save data to
pub fn save_csv_2(
    combination: HashSet<u8>,
    winners_no: Vec<usize>,
    values: Vec<u16>,
    filename: &str,
) -> Result<(), Box<dyn Error>> {
    let file = OpenOptions::new()
        .create(true)
        .write(true)
        .append(true)
        .open(filename)?;

    let mut writer = Writer::from_writer(file);

    // Write data
    // TODO make more generic
    writer.write_record(&[
        winners_no.get(0).unwrap().to_string(),
        values.get(0).unwrap().to_string(),
        values.get(1).unwrap().to_string(),
        values.get(2).unwrap().to_string(),
        winners_no.get(1).unwrap().to_string(),
        values.get(3).unwrap().to_string(),
        values.get(4).unwrap().to_string(),
        values.get(5).unwrap().to_string(),
        winners_no.get(2).unwrap().to_string(),
        values.get(6).unwrap().to_string(),
        values.get(7).unwrap().to_string(),
        values.get(8).unwrap().to_string(),
        winners_no.get(3).unwrap().to_string(),
        values.get(9).unwrap().to_string(),
        values.get(10).unwrap().to_string(),
        values.get(11).unwrap().to_string(),
        winners_no.get(4).unwrap().to_string(),
        values.get(12).unwrap().to_string(),
        values.get(13).unwrap().to_string(),
        values.get(14).unwrap().to_string(),
        winners_no.get(5).unwrap().to_string(),
        values.get(15).unwrap().to_string(),
        values.get(16).unwrap().to_string(),
        values.get(17).unwrap().to_string(),
        winners_no.get(6).unwrap().to_string(),
        values.get(18).unwrap().to_string(),
        values.get(19).unwrap().to_string(),
        values.get(20).unwrap().to_string(),
        combination
            .iter()
            .map(|&x| x.to_string())
            .collect::<Vec<String>>()
            .join(", "),
    ])?;

    writer.flush()?;
    Ok(())
}

/// Print current time to console
pub fn print_time() {
    // Get the current instant
    let current_instant = Utc::now();

    // Format the DateTime as a human-readable string
    let formatted_date_time = current_instant.format("%Y-%m-%d %H:%M:%S").to_string();

    println!("Formatted DateTime: {}", formatted_date_time);
}

/// Print how many numbers are matching
pub fn print_overlap_numb(future_win: &HashSet<u8>, combination: &(HashSet<u8>, u16, u16, u16)) {
    print!("{:?}", future_win.intersection(&combination.0).count());
}

pub fn print_colored(text: &str, color: Color) {
    let color_code = match color {
        Color::Red => "\x1B[31m",
        Color::Green => "\x1B[32m",
        Color::Yellow => "\x1B[33m",
        Color::Blue => "\x1B[34m",
        // Add more color codes as needed
    };
    println!("{}{}\x1B[0m", color_code, text);
}
