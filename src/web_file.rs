use reqwest::blocking::get;
use std::error::Error;
use std::fs::{create_dir_all, File};
use std::io::copy;
use std::path::Path;

pub fn download_file(url: String, file_name: String) -> Result<(), Box<dyn Error>> {
    // Define the path to the resources directory
    let resources_dir = Path::new("resources");

    // Create the resources directory if it doesn't exist
    if !resources_dir.exists() {
        create_dir_all(resources_dir)?;
    }

    // Define the full path to the file within the resources directory
    let file_path = resources_dir.join(file_name);

    // Send the GET request
    let response = get(url)?;

    // Ensure the request was successful
    if response.status().is_success() {
        // Open a file in write-only mode to save the downloaded file
        let mut file = File::create(&file_path)?;

        // Copy the response body to the file
        copy(&mut response.bytes()?.as_ref(), &mut file)?;

        println!(
            "File downloaded successfully and saved as '{:?}'.",
            file_path
        );
    } else {
        eprintln!("Failed to download the file. Status: {}", response.status());
    }

    Ok(())
}
