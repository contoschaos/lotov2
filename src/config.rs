#[cfg(feature = "loto")]
mod config {
    // How many columns to skip in the data file
    pub const SKIP_DATA_COLUMNS: usize = 3;
    // How many numbers are pulled in one draw
    pub const NUMBERS_PULLED: usize = 7;
    // How many numbers are guessed
    pub const NUMBERS_GUESSED: usize = 6;
    // Upper numerical bound (1 -> number_max) without any offset (49 means 49)
    pub const NUMBER_MAX: usize = 49;

    pub const NORMAL_MODE: bool = true;

    pub const SPECIAL_CASE: usize = 1;
    pub const INTERVAL: usize = 8;
}

#[cfg(feature = "vypl")]
mod config {
    // How many columns to skip in the data file
    pub const SKIP_DATA_COLUMNS: usize = 5;
    // How many numbers are pulled in one draw
    pub const NUMBERS_PULLED: usize = 6;
    // How many numbers are guessed
    pub const NUMBERS_GUESSED: usize = 6;
    // Upper numerical bound (1 -> number_max) without any offset (27 means 27)
    pub const NUMBER_MAX: usize = 27;

    pub const NORMAL_MODE: bool = true;

    pub const SPECIAL_CASE: usize = 1;
    pub const INTERVAL: usize = 6;
}

pub const BONUS_INDEX: usize = NUMBERS_PULLED - (NUMBERS_PULLED - NUMBERS_GUESSED);

#[cfg(any(feature = "loto", feature = "vypl"))]
pub use config::*;
