use std::collections::HashMap;
use std::error::Error;
use std::time::Instant;

mod data_process;
mod parallel;
mod utils;
mod web_file;

mod config;
use config::{NORMAL_MODE, NUMBER_MAX};
use data_process::{set_numbers, set_tuples};
use parallel::parallel_process;
use std::env;
use web_file::download_file;

use crate::utils::print_time;

#[cfg(feature = "loto")]
fn get_name(args: Vec<String>) -> String {
    if args.len() > 1 {
        match args[1].as_str() {
            "1" => "loto1",
            "2" => "loto2",
            _ => "loto1",
        }
    } else {
        // Default file name if no argument is provided
        "loto1"
    }
    .to_string()
}

#[cfg(feature = "vypl")]
fn get_name(args: Vec<String>) -> String {
    "extravyplata".to_string()
}

fn main() -> Result<(), Box<dyn Error>> {
    print_time();
    let mut start_time = Instant::now();

    let args: Vec<String> = env::args().collect();
    let name = get_name(args);

    let url_link = format!("https://www.tipos.sk/loterie/ciselne-loterie/informacie-k-loteriam/archiv-vyzrebovanych-cisel?file={}",name);
    let file_name = format!("{}.csv", name);
    download_file(url_link, file_name).unwrap();

    let mut end_time = Instant::now();
    let mut time_difference = end_time - start_time;
    if NORMAL_MODE {
        println!("Download time elapsed: {:?}", time_difference);
    }
    let range = if NORMAL_MODE { 0..1 } else { -2000..0 };

    // Run number of times based on mode
    for i in range.rev() {
        start_time = Instant::now();

        // Generate empty arrays with necessary size
        let mut numbers: [u16; NUMBER_MAX] = [0; NUMBER_MAX];
        let mut pairs: [[u16; NUMBER_MAX]; NUMBER_MAX + 1] = [[0; NUMBER_MAX]; NUMBER_MAX + 1];
        let mut triplets: [[[u16; NUMBER_MAX]; NUMBER_MAX]; NUMBER_MAX] =
            [[[0; NUMBER_MAX]; NUMBER_MAX]; NUMBER_MAX];
        let mut pair_tuples: HashMap<String, u16> = HashMap::new();
        let mut triplets_tuples: HashMap<String, u16> = HashMap::new();

        let last_skipped = set_numbers(
            i,
            &mut numbers,
            &mut pairs,
            &mut triplets,
            &format!("resources/{}.csv", name), // todo argument for filename
        )
        .unwrap();

        // SkipUnnecessary data and write data in tuple/map format
        set_tuples(&pairs, &triplets, &mut pair_tuples, &mut triplets_tuples);

        end_time = Instant::now();
        time_difference = end_time - start_time;
        if NORMAL_MODE {
            println!(
                "Data extraction processing time elapsed: {:?}",
                time_difference
            );
        }

        // Start parallel process
        start_time = Instant::now();
        parallel_process(
            last_skipped.iter().cloned().collect(),
            numbers,
            pair_tuples,
            triplets_tuples,
        );
        end_time = Instant::now();
        time_difference = end_time - start_time;
        if NORMAL_MODE {
            println!("Parallel time elapsed: {:?}", time_difference);
        }

        if i >= 1 {
            continue;
        }
    }
    print_time();
    Ok(())
}
